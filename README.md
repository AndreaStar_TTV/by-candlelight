# By Candlelight

By Candlelight is a videogame about reclaiming a land rendered barren by the apocalypse, growing plants, attracting bees, casting spells and dating other cute witches.
In Javascript.
From scratch.